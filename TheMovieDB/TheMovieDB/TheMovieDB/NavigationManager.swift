//
//  NavigationManager.swift
//  TheMovieDB
//
//  Created by Ankit on 04/05/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import Foundation
import UIKit

class  NavigationManager  {
    static let sharedInstance = NavigationManager()
    
    private init (){}
    
    /**
     This method is used to show list screen.
     - Returns:  No value.
     */
    func switchToMoviesListScreen()  {
        AppDelegate.shared.window = UIWindow(frame: UIScreen.main.bounds)
        if  let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MoviesListViewControllerNavigation") as? UINavigationController {
            AppDelegate.shared.window!.rootViewController = navigationController
            AppDelegate.shared.window!.makeKeyAndVisible()
        }
    }
    
    /**
     This method is used to show login screen.
     - Returns:  No value.
     */
    func switchToLoginScreen()  {
        AppDelegate.shared.window = UIWindow(frame: UIScreen.main.bounds)
        if  let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewControllerNavigation") as? UINavigationController {
            AppDelegate.shared.window!.rootViewController = navigationController
            AppDelegate.shared.window!.makeKeyAndVisible()
        }
    }
}
