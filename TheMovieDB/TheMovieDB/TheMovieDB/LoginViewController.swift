//
//  LoginViewController.swift
//  TheMovieDB
//
//  Created by Ankit on 04/05/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLoginDidClicked(_ sender: Any) {
        let email = txtEmail.text
        let password = txtPassword.text
        
        if email != "" && password != "" {
            Auth.auth().signIn(withEmail: email!, password: password!) { user, error in
                if let error = error, user == nil {
                    self.loginErrorAlert(title: "Oops!", message: error.localizedDescription)
                }else{
                    UserDefaults.standard.setValue(user?.user.uid, forKey: "uid")
                    NavigationManager.sharedInstance.switchToMoviesListScreen()
                }
            }
        } else {
            loginErrorAlert(title: "Oops!", message: "Don't forget to enter your email and password.")
        }
        
    }
    
    func loginErrorAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSignUpDidClicked(_ sender: Any) {
        let signUpVC = storyboard!.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
       
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    @IBAction func forgetPasswordDidClicked(_ sender: Any) {
        let forgetVC = storyboard!.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        
        self.navigationController?.pushViewController(forgetVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
