//
//  BaseCollectionViewCell.swift
//  TheMovieDB
//
//  Created by Ankit on 30/04/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    static var identifier: String {
        return String(describing: self)
    }
    
    var isCellSelected: Bool = false
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
