//
//  VideoCollectionViewCell.swift
//  TheMovieDB
//
//  Created by Ankit on 05/05/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class VideoCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lbSize: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(_ movie: MovieVideo) {
        lbltitle.text = movie.name
        lbSize.text = String(movie.size) + "p"
    }
}
