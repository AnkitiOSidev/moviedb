//
//  ViewController.swift
//  TheMovieDB
//
//  Created by Ankit on 30/04/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit
import Firebase

class MoviesListViewController: UIViewController {
    
    let animationDuration: TimeInterval = 0.3
    let listLayoutStaticCellHeight: CGFloat = 100
    let gridLayoutStaticCellHeight: CGFloat = (((UIScreen.main.bounds.width/2) - CGFloat(2 * 6.0)) / 0.88)
    
    fileprivate var isTransitionAvailable = true
    
    @IBOutlet weak var collectionViewCategories: MIHInfiniteScrollView!
    @IBOutlet weak var collectionViewMovies: UICollectionView!
    
    let endpoints: [Endpoint] = [.nowPlaying, .upcoming, .popular, .topRated]
    
    fileprivate lazy var listLayout = DisplaySwitchLayout(staticCellHeight: listLayoutStaticCellHeight, nextLayoutStaticCellHeight: gridLayoutStaticCellHeight, layoutState: .list)
    fileprivate lazy var gridLayout = DisplaySwitchLayout(staticCellHeight: gridLayoutStaticCellHeight, nextLayoutStaticCellHeight: listLayoutStaticCellHeight, layoutState: .grid,cellPadding: CGPoint(x: 6, y: 6), gridLayoutCountOfColumns: 2)
    fileprivate var layoutState: LayoutState = .list
    
    var endpoint: Endpoint?
    var movieService: MovieService = MovieStore.shared
    var totalRecords = 0
    var page = 1
    var movies = [Movie]() {
        didSet {
            collectionViewMovies.reloadData()
        }
    }
    var button = UIButton.init(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        endpoint = endpoints[0]
        self.title = "Movies List"
        addRightbarButton()
        setupInfiniteScrollView()
        setUICollection()
        fetchMovies()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func setUICollection(){
        self.collectionViewMovies.backgroundColor = UIColor.clear
        collectionViewMovies.collectionViewLayout = listLayout
        
        collectionViewMovies.register(MoviesGridCollectionViewCell.nib, forCellWithReuseIdentifier: MoviesGridCollectionViewCell.identifier)
        collectionViewMovies.register(MoviesListCollectionViewCell.nib, forCellWithReuseIdentifier: MoviesListCollectionViewCell.identifier)
        
        collectionViewMovies.delegate = self
        collectionViewMovies.dataSource = self
    }

    @IBAction func btnListDidClicked(_ sender: Any) {
        if !isTransitionAvailable {
            return
        }
        let transitionManager: TransitionManager
        if layoutState == .list {
            layoutState = .grid
            transitionManager = TransitionManager(duration: animationDuration, collectionView: collectionViewMovies!, destinationLayout: gridLayout, layoutState: layoutState)
        } else {
            layoutState = .list
            transitionManager = TransitionManager(duration: animationDuration, collectionView: collectionViewMovies!, destinationLayout: listLayout, layoutState: layoutState)
        }
        transitionManager.startInteractiveTransition()
    }
    
    private func fetchMovies() {
        guard let endpoint = endpoint else {
            return
        }
        
        movieService.fetchMovies(from: endpoint, params: ["page": String(page)], successHandler: { [unowned self] (response) in
            self.totalRecords = response.totalResults
            self.page = response.page
            self.movies.append(contentsOf: response.results)
        }) { [unowned self] (error) in
        }
    }
    
    func addRightbarButton() {
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem()]
        addLogoutButton()
        button.frame = CGRect(x: 0.0, y: 5.0, width: 50.0, height: 32.0)
        button.setTitle("Grid", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(switchList(button:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: button))
    }
    
    func addLogoutButton()  {
        let button = UIButton.init(type: .custom)
        button.frame = CGRect(x: 0.0, y: 5.0, width: 50.0, height: 32.0)
        button.setTitle("Logout", for: .normal)
        button.setTitleColor(.red, for: .normal)
        button.addTarget(self, action: #selector(logout(button:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: button))
    }
    
    @objc func logout(button: UIButton) {
        UserDefaults.standard.setValue(nil, forKey: "uid")
        NavigationManager.sharedInstance.switchToLoginScreen()
    }
    
    @objc func switchList(button: UIButton) {
        if !isTransitionAvailable {
            return
        }
        let transitionManager: TransitionManager
        if layoutState == .list {
            button.setTitle("List", for: .normal)
            layoutState = .grid
            transitionManager = TransitionManager(duration: animationDuration, collectionView: collectionViewMovies!, destinationLayout: gridLayout, layoutState: layoutState)
        } else {
            button.setTitle("Grid", for: .normal)
            layoutState = .list
            transitionManager = TransitionManager(duration: animationDuration, collectionView: collectionViewMovies!, destinationLayout: listLayout, layoutState: layoutState)
        }
        transitionManager.startInteractiveTransition()
    }
}


extension MoviesListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, transitionLayoutForOldLayout fromLayout: UICollectionViewLayout, newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
        return customTransitionLayout
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isTransitionAvailable = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isTransitionAvailable = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movieDetailVC = storyboard!.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        movieDetailVC.title = movies[indexPath.item].title
        movieDetailVC.movieId = movies[indexPath.item].id
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }
}

extension MoviesListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if ((indexPath.row == (movies.count-1)) && movies.count < totalRecords) {
            page = page + 1
            self.fetchMovies()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if layoutState == .grid {
            if let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: MoviesGridCollectionViewCell.identifier, for: indexPath) as? MoviesGridCollectionViewCell {
                cell.configure(movies[indexPath.row])
                return cell
            }
        } else {
            if let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: MoviesListCollectionViewCell.identifier, for: indexPath) as? MoviesListCollectionViewCell {
                cell.configure(movies[indexPath.row])
                return cell
            }
        }
        return UICollectionViewCell()
    }
}

// MARK: - MIHInfiniteScrollDelegate
extension MoviesListViewController: MIHInfiniteScrollDelegate {
    func setupInfiniteScrollView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 100, height:40 )
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        collectionViewCategories.collectionViewLayout = layout
        
        
        var cellStyle = CellStyle()
        cellStyle.titleFont = UIFont.systemFont(ofSize: 15.0)
        cellStyle.selectedTitleColor = UIColor.purple
        cellStyle.normalTitleColor = UIColor.black
        cellStyle.indicatorColor = UIColor.purple
        cellStyle.isCellWidthDynamic = false
        collectionViewCategories.infiniteScrollDelegate = self
        
        let categories = endpoints.map({$0.description})
        if categories.count > 3 {
            cellStyle.isCellWidthDynamic = true
        }
        collectionViewCategories.setupCollection(menuArray: categories, cellStyle: cellStyle)
        
    }
    
    func navigateScreenBy(index: Int) {
        endpoint =  endpoints[index]
        self.totalRecords = 0
        self.page = 1
        self.movies.removeAll()
        fetchMovies()
    }
}
