//
//  MovieDetailViewController.swift
//  TheMovieDB
//
//  Created by Ankit on 03/05/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit
import XCDYouTubeKit
import AVKit

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var taglineLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var adultLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var castLabel: UILabel!
    @IBOutlet weak var crewLabel: UILabel!
    @IBOutlet weak var imageViewTop: UIImageView!
    var isFavourite = false
    var movieService: MovieService = MovieStore.shared
    var movieId: Int!
    
    @IBOutlet weak var collectionViewVideos: UICollectionView!
    @IBOutlet weak var btnAddToFavourite: UIButton!
    
    @IBOutlet weak var videoHeightConstraint: NSLayoutConstraint!
    let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        return dateFormatter
    }()
    
    private var movie: Movie! {
        didSet {
            updateMovieDetail()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollview.isHidden = true
        btnAddToFavourite.isHidden = true
        fetchMovieDetail()
        setUICollection()
        // Do any additional setup after loading the view.
    }
    
    func setUICollection(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        layout.itemSize = CGSize(width: 135, height:180)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        collectionViewVideos.collectionViewLayout = layout
        collectionViewVideos.contentOffset = CGPoint(x: 0, y: 0)
        collectionViewVideos.showsHorizontalScrollIndicator = false
        
    self.collectionViewVideos.backgroundColor = UIColor.clear

    collectionViewVideos.register(VideoCollectionViewCell.nib, forCellWithReuseIdentifier: VideoCollectionViewCell.identifier)
    
        collectionViewVideos.delegate = self
        collectionViewVideos.dataSource = self
    }
    
    private func fetchMovieDetail() {
        
        guard let movieId = movieId else {
            return
        }
       
        movieService.fetchMovie(id: movieId, successHandler: {[weak self] (movie) in
            self?.movie = movie
        }) { [weak self] (error) in
        }
    }
    
    private func updateMovieDetail() {
        guard let movie = movie else {
            return
        }
        
        DataService.dataService.checkMovieFavourite(movieId: String(movieId), completion: { [weak self] (isFavourite) in
            guard let wself = self else {
                return
            }
            wself.isFavourite = isFavourite
            if  isFavourite {
            wself.btnAddToFavourite.setTitle("Remove Favorite", for: .normal)
            }else{
            wself.btnAddToFavourite.setTitle("Add Favorite", for: .normal)
            }
            wself.btnAddToFavourite.isHidden = false
        })
        
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: movie.posterURL)
        imageViewTop.kf.setImage(with: movie.posterURL)
        taglineLabel.text = movie.tagline
        overviewLabel.text = movie.overview
        yearLabel.text = self.dateFormatter.string(from: movie.releaseDate)
        if movie.voteCount == 0 {
            ratingLabel.isHidden = true
        } else {
            ratingLabel.isHidden = false
            ratingLabel.text = movie.voteAveragePercentText
        }
        
        adultLabel.isHidden = !movie.adult
        
        durationLabel.text = "\(movie.runtime ?? 0) mins"
        if let genres = movie.genres, genres.count > 0 {
            genreLabel.isHidden = false
            genreLabel.text = "Genre: " + genres.map { $0.name }.joined(separator: ", ")
        } else {
            genreLabel.isHidden = true
        }
        
        if let casts = movie.credits?.cast, casts.count > 0 {
            castLabel.isHidden = false
            castLabel.text = "Cast: \(casts.prefix(upTo: 3).map { $0.name }.joined(separator: ", "))"
        } else {
            castLabel.isHidden = true
        }
        
        if let director = movie.credits?.crew.first(where: {$0.job == "Director"}) {
            crewLabel.isHidden = false
            crewLabel.text = "Director: \(director.name)"
        } else {
            crewLabel.isHidden = true
        }
        if (movie.videos?.results.count ?? 0) > 0 {
            videoHeightConstraint.constant = 220
            collectionViewVideos.reloadData()
        }
        
        scrollview.isHidden = false
    }
    
    @IBAction func btnAddFavouriteDidClicked(_ sender: Any) {
        isFavourite = !isFavourite
        DataService.dataService.changeFavoriteList(movieId: String(movieId), isFavourite: isFavourite)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MovieDetailViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let video = (movie?.videos?.results ?? [])[indexPath.row]
        let playerVC = AVPlayerViewController()
        
        present(playerVC, animated: true, completion: nil)
        XCDYouTubeClient.default().getVideoWithIdentifier(video.key) {[weak self, weak playerVC] (video, error) in
            if let _ = error {
                self?.dismiss(animated: true, completion: nil)
                return
            }
            guard let video = video else {
                self?.dismiss(animated: true, completion: nil)
                
                return
            }
            
            let streamURL: URL
            if let url = video.streamURLs[XCDYouTubeVideoQuality.HD720.rawValue]  {
                streamURL = url
            } else if let url = video.streamURLs[XCDYouTubeVideoQuality.medium360.rawValue] {
                streamURL = url
            } else if let url = video.streamURLs[XCDYouTubeVideoQuality.small240.rawValue] {
                streamURL = url
            } else if let urlDict = video.streamURLs.first {
                streamURL = urlDict.value
            } else {
                self?.dismiss(animated: true, completion: nil)
                
                return
            }
            playerVC?.player = AVPlayer(url: streamURL)
            playerVC?.player?.play()
            
        }
    }
}

extension MovieDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return movie?.videos?.results.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: VideoCollectionViewCell.identifier, for: indexPath) as? VideoCollectionViewCell {
            cell.configure(((movie?.videos?.results ?? [])[indexPath.row]))
            cell.imgMovie.kf.setImage(with: movie.posterURL)
            return cell
        }
        return UICollectionViewCell()
    }
    
    
}
