//
//  ForgetPasswordViewController.swift
//  TheMovieDB
//
//  Created by Ankit on 05/05/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit
import Firebase

class ForgetPasswordViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSendDidClicked(_ sender: Any) {
        let email = txtEmail.text
       
        if email != "" {
            Auth.auth().sendPasswordReset(withEmail: email!) { (error) in
                if let error = error {
                    self.loginErrorAlert(title: "Oops!", message: error.localizedDescription)
                }else{
                    self.loginErrorAlert(title: "Hey!", message: "Emali sent to your account. Please check your email")
                }
            }
        } else {
            loginErrorAlert(title: "Oops!", message: "Don't forget to enter your email and password.")
        }
        
    }
    
    func loginErrorAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
