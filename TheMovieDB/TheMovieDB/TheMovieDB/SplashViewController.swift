//
//  SplashViewController.swift
//  TheMovieDB
//
//  Created by Ankit on 04/05/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.value(forKey: "uid") != nil && DataService.dataService.CURRENT_USER_REF != nil {
            NavigationManager.sharedInstance.switchToMoviesListScreen()
        }else{
            NavigationManager.sharedInstance.switchToLoginScreen()
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
