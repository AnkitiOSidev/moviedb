//
//  DataService.swift
//  TheMovieDB
//
//  Created by Ankit on 04/05/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class DataService {
    static let BASE_URL = "https://movieapp-b3434.firebaseio.com/"
    
    static let dataService = DataService()
    
    private var _BASE_REF = Database.database().reference(fromURL: BASE_URL)
    private var _USER_REF = Database.database().reference(fromURL: "\(BASE_URL)/users")
    private var _FAVORITE_LIST_MOVIE_REF = Database.database().reference(fromURL: "\(BASE_URL)/favoritemovies")
    
    var BASE_REF: DatabaseReference {
        return _BASE_REF
    }
    
    var USER_REF: DatabaseReference {
        return _USER_REF
    }
    
    var CURRENT_USER_REF: DatabaseReference? {
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        let currentUser = _BASE_REF.child("users").child(userID)
        return currentUser
    }
    
    var FAVORITE_LIST_MOVIE_REF: DatabaseReference {
        return _FAVORITE_LIST_MOVIE_REF
    }
    
    func createNewAccount(uid: String, user: Dictionary<String, String>) {
        // A User is born.
        USER_REF.child(uid).setValue(user)
        //USER_REF.child(byAppendingPath: uid).setValue(user)
    }
    
    func changeFavoriteList(movieId: String, isFavourite: Bool) {
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        FAVORITE_LIST_MOVIE_REF.child(userID).child(movieId).setValue(isFavourite)
    }
  
    func checkMovieFavourite(movieId : String,completion:@escaping (Bool) -> ()) {
        let userID = UserDefaults.standard.value(forKey: "uid") as! String
        FAVORITE_LIST_MOVIE_REF.child(userID).child(movieId).observe(.value) { (snapshot) in
            return completion(snapshot.value as? Bool ?? false)
        }
    }
}
